clc;
clear all;
close all;

setup;
set(0,'DefaultFigureVisible','on');

obj_data        = data(matbox_path); 
obj_observables = observables;
obj_cluster     = cluster;
obj_store       = store(obj_data);

obj_data.initMovie(1);

fprintf('END_STEP=%.2f \n',obj_data.END_STEP);
fprintf('TIME_STEP_SKIP=%.2f \n',obj_data.TIME_STEP_SKIP);


istore = 1;
 for iter=451
% for iter=1:obj_data.TIME_STEP_SKIP:obj_data.END_STEP
%for iter=10%obj_data.END_STEP%1693%
    
    if(obj_data.TIME_BASE==1)
       step = iter; 
    else
       step = floor(obj_data.TIME_BASE^iter)+1;    
    end
    
    pause(1e-16);
    
    timestep = (step-1)*obj_data.ITER_DATA_GEN;
    time     = timestep*obj_data.DELTA_T
    istore              
    
    obj_data = obj_data.loadData(step);
    
    obj_observables.observablesMain(obj_data, time, iter);    
    
    obj_cluster.clusterMain(obj_data, step, time);        
    
    istore=obj_store.store2memory(obj_cluster, obj_observables, time, istore);

    if (obj_data.MOVIE)
%         frame = getframe(obj_observables.fig_1_gcf);     
          frame = getframe(obj_cluster.fig_cls_04_gcf);    
          writeVideo(obj_data.video_name, frame);
    end            
    
    if(obj_cluster.SHOW_COLORED_CLUSTERS)
    hold(obj_cluster.fig_cls_04,'off');
    end
    
    max(obj_data.momentX)
    
end

if (obj_data.MOVIE)
    close(obj_data.video_name);
end

pause(1);
obj_store.store2files();
pause(1);
%|end|