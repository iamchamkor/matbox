classdef sph < handle
   
   properties

      positionX;
      positionY;
      positionZ;
      velocityX;
      velocityY;
      velocityZ;
      charge;
      energy;
      density;
      forceX;
      forceY;
      forceZ;
     
      energy_switch;
      density_switch;
      charge_switch;
      force_switch;
            
      matbox_path;
      sim_title;
      sim_directory;
      out_directory;
            
      listing;   
      
      MOVIE;       
      video_name;
      
      TIME_BASE; 
      END_STEP;     
      N_PARTICLE;
      DIAMETER_PARTICLE;
      ITER_DATA_GEN;
      DELTA_T;
      TIME_STEP_SKIP;
      FLOATTYPE; 
      CELL_LENGTH;
      VIEW_ANGLE;
      PARTICLE_SKIP;
      
      T_HAFF_YSHIFT;
      TAU_INV;
      HAFFS_EXPONENT;      
      N_BOXES_X;
      
   end
   
   methods
      function [obj] = sph(matbox_path)%class-constructor

           %|switches|
           obj.energy_switch  = 0;
           obj.density_switch = 0;
           obj.charge_switch  = 0;
           obj.force_switch   = 0;           

           %|path setup|
           obj.matbox_path    = matbox_path;
                                 
           obj.sim_directory  ='/scratch.local/data/data-galemd-output/3D_N50016_L70.0_CCR0.85_PHI0.076_EC277.21_DC27.71_KMAX8_RANDQ1.00e+00_CP1.0000_BOTHRK_1_test_for_interstitial_fluid';           
           obj.sim_title      ='test_for_interstitial_fluid';

           obj.N_PARTICLE      = 343000;

           obj.out_directory = sprintf('%s/out-sph/%s/', obj.matbox_path, obj.sim_title);               
               if ~exist( obj.out_directory, 'dir')
                    mkdir(obj.out_directory);           
               else                    
                    delete(sprintf('%s/*.*',obj.out_directory));      
               end             
                             
           %|file listing|
           listing      = dir(strcat((obj.sim_directory),'/positionX_fluid_particle_t*.dat'));
           listingName  = {listing(:).name};
           listing      = natsortfiles(listingName);    
           obj.listing  = listing;
 
           %|extraction time steps, lin/log time extraction|
           %obj.TIME_BASE    = 1.4;
           obj.TIME_BASE    = 1.0;           
           if(obj.TIME_BASE==1)
               obj.END_STEP = length(listing);
           else
               obj.END_STEP = floor(log(length(listing))/log(obj.TIME_BASE));
           end           
           
           %|sim parameters|

           obj.DIAMETER_PARTICLE       = 1.0;
           obj.ITER_DATA_GEN           = 1000;
           obj.DELTA_T                 = 0.001;
           obj.TIME_STEP_SKIP          = 1;
           obj.FLOATTYPE               = 'float';
           obj.CELL_LENGTH             = 70;
           obj.VIEW_ANGLE              = 30;
           obj.PARTICLE_SKIP           = 1;
                      
           obj.T_HAFF_YSHIFT           = 1000.0;
           obj.TAU_INV                 = 10.0;
           obj.HAFFS_EXPONENT          = -5/3;
           
           obj.N_BOXES_X               = 12;               
           
       end
       
       function [] = initMovie(obj, movie_switch)                      
           obj.MOVIE = movie_switch;
           if(obj.MOVIE)
               obj.video_name = obj.sim_title;
               obj.video_name = VideoWriter( sprintf('%s/%s.avi', obj.out_directory, obj.video_name) );
               open(obj.video_name);
           end
       end
       
   end
   
end