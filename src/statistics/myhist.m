function [] = myhist(fig_handle,x)

	N     = length(x);   
	
    nbins = round(sqrt(N));
    
    [Count,xi] = hist(x,nbins);
   
	Pi = Count/trapz(xi,Count);

 	plot(fig_handle,xi, Pi, '-');
    
    %histogram(fig_handle, x, round(sqrt(length(x))), 'Normalization','probability');
    
end
