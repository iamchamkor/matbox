function [] = trajectory(fig_handle,positionX,positionY,positionZ,pid,step)

    global VIEW_ANGLE;
    global CELL_LENGTH;

    if(fig_handle~=0)
        traj_X(step) = positionX(pid);
        traj_Y(step) = positionY(pid);
        traj_Z(step) = positionZ(pid);
        plot3(fig_handle,traj_X,traj_Y,traj_Z,'o');
        box(fig_handle,'on'); fig_handle.BoxStyle = 'full';
        set(fig_handle,'View',[-VIEW_ANGLE,VIEW_ANGLE]);
        axis(fig_handle,[-CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2]);
    end
    
end