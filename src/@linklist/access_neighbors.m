function [] = access_neighbors(linklist, data, head, next)
    
    NX = data.N_BOXES_X;
    NY = data.N_BOXES_X;
    NZ = data.N_BOXES_X;

    NP = data.N_PARTICLE;
    
    LX = data.CELL_LENGTH;
    LY = data.CELL_LENGTH;
    LZ = data.CELL_LENGTH;

%     icut = params.intr_cut;
% 
%     for(pid=1:data.N_PARTICLE)
%         particle[pid].fx = 0.0;
%         particle[pid].fy = 0.0;
%         particle[pid].fz = 0.0;
%     end

    % Scan inner cells
    for (ix_cell=0:NX-1)
    for (iy_cell=0:NY-1)
    for (iz_cell=0:NZ-1)

    % Calculate a scalar cell index
    i_cell = ix_cell + NX*iy_cell + NX*NY*iz_cell + 1; % +1 to match matlab indexing

        % Scan the neighbor cells (including itself) of cell c
        for (ix_cell_neighbour=ix_cell-1 : ix_cell+1)
        for (iy_cell_neighbour=iy_cell-1 : iy_cell+1)
        for (iz_cell_neighbour=iz_cell-1 : iz_cell+1)

        rshift_x = 0.0; 
        rshift_y = 0.0; 
        rshift_z = 0.0;
        
        % Periodic boundary condition by shifting coordinates
        if(ix_cell_neighbour < 0)
            rshift_x = -LX;        
        elseif(ix_cell_neighbour >= NX)
            rshift_x = +LX;        
        else
            rshift_x = 0.0;
        end

        if(iy_cell_neighbour < 0)
            rshift_y = -LY;        
        elseif(iy_cell_neighbour >= NY)
            rshift_y = +LY;        
        else
            rshift_y = 0.0;
        end

        if(iz_cell_neighbour < 0)
            rshift_z = -LZ;        
        elseif(iz_cell_neighbour >= NZ)
            rshift_z = +LZ;        
        else
            rshift_z = 0.0;
        end

        % Calculate the scalar cell index of the neighbor cell
        i_cell_neighbour = rem((iz_cell_neighbour+NZ),NZ)*(NX*NY)...
                              +rem((iy_cell_neighbour+NY),NY)*(NX)...
                                +rem((ix_cell_neighbour+NX),NX)...
                                  +1; % +1 to match matlab indexing
                            
        % Scan particle i in cell c
        i = head(i_cell);
        while (i ~= -1)

            % Scan particle j in cell c1
            j = head(i_cell_neighbour);
            while (j ~= -1) 

                if (i < j) % to avoid double counting of pair (i, j)

                    rx_ji = data.positionX(i) - (data.positionX(j) + rshift_x); % Image-corrected relative pair position, r_ji = r_i - r_j in the direction from particle j to i
                    ry_ji = data.positionY(i) - (data.positionY(j) + rshift_y); % Image-corrected relative pair position, r_ji = r_i - r_j in the direction from particle j to i
                    rz_ji = data.positionZ(i) - (data.positionZ(j) + rshift_z); % Image-corrected relative pair position, r_ji = r_i - r_j in the direction from particle j to i

                    r_ji = sqrt(rx_ji*rx_ji + ry_ji*ry_ji + rz_ji*rz_ji);

                    if (r_ji <= 1.0)
                        
                        %Compute forces on pair (i, j)

                        overlap = data.DIAMETER_PARTICLE - r_ji;

                        if(overlap>0)

                          overlap


                        end
                    end
                end

                j = next(j);
            end

            i = next(i);
        end

        end
        end
        end

    end
    end
    end
    
end