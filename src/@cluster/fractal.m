function [Rg,Df] = fractal(clusters, cid)

    mean_x = mean(clusters{1,cid}(clusters{4,cid}));
    mean_y = mean(clusters{2,cid}(clusters{4,cid}));
    mean_z = mean(clusters{3,cid}(clusters{4,cid}));
    
    r_mean = [mean_x, mean_y, mean_z];
      
    position_mat = [clusters{1,cid}(clusters{4,cid}), clusters{2,cid}(clusters{4,cid}), clusters{3,cid}(clusters{4,cid})];
        
    rel_position = position_mat - r_mean;    
    
    %distance_mat = squareform(pdist(position_mat)); % here Nm - your matrix [Nx3]
    
    N=length(clusters{4,cid});
    
    Ro = 0.5;
    
    %Rg = sqrt(sum(sum(distance_mat.^2))/(2*N^2));
    
    Rg = sqrt(sum( rel_position(:,1).^2 + rel_position(:,2).^2 + rel_position(:,3).^2 )/N);
    
    Df = log(N)/log(Rg/Ro);  

end