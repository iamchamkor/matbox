function [avgmass] = avgclustermass(m)

	N     = length(m);   
	
    nbins = round(sqrt(N));
    
    [Count,i] = hist(m,nbins);
   
	ci = Count/trapz(i,Count);

	M1     = sum((i.^1).*ci); %1st raw moment around 0;    
	M2     = sum((i.^2).*ci); %2nd raw moment around 0;
    
    avgmass = M2/M1;
    
end
