function [] = getClusterDetails(cluster,data,fig_handle,time)

positionX_rest = data.positionX;
positionY_rest = data.positionY;
positionZ_rest = data.positionZ;

velocityX_rest = data.velocityX;
velocityY_rest = data.velocityY;
velocityZ_rest = data.velocityZ;

charge_rest    = data.charge;

momentX_rest   = data.momentX;
momentY_rest   = data.momentY;
momentZ_rest   = data.momentZ;



cluster.no_of_particles_in_ith_cluster  = zeros();
cid                                     = 0;
cid_2                                   = 0;
tracked_particles                       = 0;

cluster.cid_maxsize      = 1;
cluster.max_cluster_mass = 0;

cluster.Rg=zeros();
cluster.Df=zeros();
% T_agg=zeros();

pos_cls_x=zeros();
pos_cls_y=zeros();
pos_cls_z=zeros();
vel_cls_x=zeros();
vel_cls_y=zeros();
vel_cls_z=zeros();
cluster.q_cls=zeros();
mom_cls_x=zeros();
mom_cls_y=zeros();
mom_cls_z=zeros();
m_cls=zeros();

cluster.clusters = {};

while( length(positionX_rest)>0 )
    
    i_start = 1;
    
    [nbrs_cumulative] = neighbours(fig_handle, positionX_rest, positionY_rest, positionZ_rest, i_start, [i_start;], cluster.CONTACT_DISTANCE);

    cid = cid+1;
    tracked_particles = tracked_particles + length(nbrs_cumulative);
    
    
    if( (length(nbrs_cumulative)>=cluster.MINIMUM_PARTICLES) && (length(nbrs_cumulative)<=cluster.MAXIMUM_PARTICLES) )        
        
        cid_2 = cid_2+1;      
        
        cluster.no_of_particles_in_ith_cluster(cid_2) = length(nbrs_cumulative);
        
        if(cluster.SHOW_COLORED_CLUSTERS)
        data.scatter3_handle = domain(fig_handle,positionX_rest(nbrs_cumulative),...
                                    positionY_rest(nbrs_cumulative),...
                                        positionZ_rest(nbrs_cumulative),'random_color','blue','show_axis_lines',time);    
        hold(fig_handle,'on');
        end
                            
        cluster.clusters{1,cid_2} = positionX_rest;
        cluster.clusters{2,cid_2} = positionY_rest;
        cluster.clusters{3,cid_2} = positionZ_rest;
        
        cluster.clusters{4,cid_2} = nbrs_cumulative;       
        
        cluster.clusters{5,cid_2} = velocityX_rest;
        cluster.clusters{6,cid_2} = velocityY_rest;
        cluster.clusters{7,cid_2} = velocityZ_rest;
        
        cluster.clusters{8,cid_2} = charge_rest;
        
        cluster.clusters{9,cid_2} = momentX_rest;
        cluster.clusters{10,cid_2}= momentY_rest;
        cluster.clusters{11,cid_2}= momentZ_rest;
        
        [cluster.Rg(cid_2),cluster.Df(cid_2)] = fractal(cluster.clusters,cid_2);  
        
%         [T_agg(cid_2)] = temperature2(clusters, cid_2);
        
        [pos_cls_x(cid_2),pos_cls_y(cid_2),pos_cls_z(cid_2),vel_cls_x(cid_2),vel_cls_y(cid_2),vel_cls_z(cid_2),cluster.q_cls(cid_2),mom_cls_x(cid_2),mom_cls_y(cid_2),mom_cls_z(cid_2),m_cls(cid_2)] ...
            = cluster2particle(cluster.clusters, cid_2);

        
        if(length(nbrs_cumulative) > cluster.max_cluster_mass)            
            cluster.cid_maxsize      = cid_2;               
            cluster.max_cluster_mass = length(nbrs_cumulative);
        end
                                
    end
    
    %length(nbrs_cumulative)
    %tracked_particles
    
    positionX_rest(nbrs_cumulative) = [];
    positionY_rest(nbrs_cumulative) = [];
    positionZ_rest(nbrs_cumulative) = [];
    
    velocityX_rest(nbrs_cumulative) = [];
    velocityY_rest(nbrs_cumulative) = [];
    velocityZ_rest(nbrs_cumulative) = [];
    
    charge_rest(nbrs_cumulative)    = [];
    
    momentX_rest(nbrs_cumulative)   = [];
    momentY_rest(nbrs_cumulative)   = [];
    momentZ_rest(nbrs_cumulative)   = [];    
    
end

cluster.no_of_clusters   = cid_2;
cluster.max_cluster_size = cluster.max_cluster_mass^(1/2.5);
cluster.avg_cluster_mass = avgclustermass(cluster.no_of_particles_in_ith_cluster);
cluster.avg_cluster_size = cluster.avg_cluster_mass^(1/2.5);

[cluster.T_cls] = cluster.granularTemperaturePreciseCluster2Particle(data,vel_cls_x,vel_cls_y,vel_cls_z,pos_cls_x,pos_cls_y,pos_cls_z,cluster.q_cls,m_cls);



cluster.v_cls_abs   = sqrt(vel_cls_x.^2 + vel_cls_y.^2 + vel_cls_z.^2);
cluster.mom_cls_abs = sqrt(mom_cls_x.^2 + mom_cls_y.^2 + mom_cls_z.^2);

cluster.var_mom_cls = varClusterMoment(mom_cls_x,mom_cls_y,mom_cls_z);

if(cluster.SHOW_CLUSTER_MOMENTS)
    quiver3(fig_handle,pos_cls_x,pos_cls_y,pos_cls_z,mom_cls_x,mom_cls_y,mom_cls_z);                             
end 

%          domain(fig_handle,pos_cls_x,...
%                               pos_cls_y,...
%                                  pos_cls_z,'random_color','blue','show_axis_lines');


end
