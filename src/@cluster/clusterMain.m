function [] = clusterMain(cluster, data, step, time)

        if(cluster.CLUSTER)
            
            iter=step;

            MINIMUM_PARTICLES = cluster.MINIMUM_PARTICLES;
                                    
            cluster.getClusterDetails(data,cluster.fig_cls_04,time);                   
        
        end
                                          
        if(cluster.MASS_VS_RG)
            p1 = plot(cluster.fig_37, cluster.no_of_particles_in_ith_cluster, cluster.Rg,'or','markersize', 5, 'markerfacecolor', 'none');
            [Df_fit, Rg_fit] = fractalDimensionLinearFit(cluster, cluster.no_of_particles_in_ith_cluster, cluster.Rg);
            p2 = plot(cluster.fig_37, cluster.no_of_particles_in_ith_cluster, Rg_fit, '- b');
            set(p2,'MarkerFaceColor', get(p1,'Color'));
            set(cluster.fig_37,'xlim',[1 1000],'ylim',[0.1 100]);            
            x1 = 2;
            y1 = 13;            
            txt1 = sprintf('$t=%.2f, D_f=%.2f$', time, Df_fit);
            leg1=text(cluster.fig_37, x1, y1, txt1);
            set(leg1, 'interpreter', 'latex', 'fontsize', 20);
            cluster.Df_fit = Df_fit;
        end

        if(cluster.MASS_VS_VELOCITY)
            p1 = plot(cluster.fig_cls_02, cluster.no_of_particles_in_ith_cluster, cluster.v_cls_abs,'or','markersize', 5, 'markerfacecolor', 'none');
            [Alpha_fit, v_cls_abs_fit] = alphaLinearFit(cluster, cluster.no_of_particles_in_ith_cluster, cluster.v_cls_abs);
            p2 = plot(cluster.fig_cls_02, cluster.no_of_particles_in_ith_cluster, v_cls_abs_fit, '- b');
            set(p2,'MarkerFaceColor', get(p1,'Color'));
            %set(cluster.fig_37,'xlim',[1 1000],'ylim',[0.1 100]);            
            x1 = 2;
            y1 = 13;            
            txt1 = sprintf('$t=%.2f, D_f=%.2f$', time, Alpha_fit);
            leg1=text(cluster.fig_cls_02, x1, y1, txt1);
            set(leg1, 'interpreter', 'latex', 'fontsize', 20);
            cluster.Alpha_fit = Alpha_fit;
        end        

        if(cluster.MASS_VS_CHARGE)
            p1 = plot(cluster.fig_cls_03, cluster.no_of_particles_in_ith_cluster, abs(cluster.q_cls),'or','markersize', 5, 'markerfacecolor', 'none');
            [Beta_fit, q_cls_fit] = betaLinearFit(cluster, cluster.no_of_particles_in_ith_cluster, abs(cluster.q_cls));
            p2 = plot(cluster.fig_cls_03, cluster.no_of_particles_in_ith_cluster, q_cls_fit, '- b');
            set(p2,'MarkerFaceColor', get(p1,'Color'));
            %set(cluster.fig_37,'xlim',[1 1000],'ylim',[0.1 100]);            
            x1 = 2;
            y1 = 13;            
            txt1 = sprintf('$t=%.2f, D_f=%.2f$', time, Beta_fit);
            leg1=text(cluster.fig_cls_03, x1, y1, txt1);
            set(leg1, 'interpreter', 'latex', 'fontsize', 20);
            
            cluster.q_cls_abs = abs(cluster.q_cls);
            cluster.Beta_fit  = Beta_fit;
        end          
        
        if(cluster.SHOW_CLUSTER_MIN_PARTICLES)
            filename   = sprintf('n_min_%d_t_%d',MINIMUM_PARTICLES,time);
            printtitle = sprintf('$N_{cl} > %d,D_{f,avg}=%.2f,t=%d$',MINIMUM_PARTICLES,Df_avg,time);
            title(cluster.fig_29,printtitle,'interpreter','latex');
            printpdf(cluster.fig_29_gcf,filename);
        end
        

        if(cluster.SHOW_MAX_CLUSTER)
            domain(cluster.fig_24,cluster.clusters{1,cluster.cid_maxsize}(cluster.clusters{4,cluster.cid_maxsize}),...
            cluster.clusters{2,cluster.cid_maxsize}(cluster.clusters{4,cluster.cid_maxsize}),...
            cluster.clusters{3,cluster.cid_maxsize}(cluster.clusters{4,cluster.cid_maxsize}),'fixed_color', 'blue', 'dont_show_axis_lines');            
        
        
        Df_maxsize = Df(cid_maxsize);        
        filename   = sprintf('t_%d',time);
        printtitle = sprintf('$D_f=%.2f, t=%d$',Df_maxsize,time);        
        title(cluster.fig_24,printtitle,'interpreter','latex');         
        printpdf(cluster.fig_24_gcf,filename);            
        end
        

        if(cluster.CLUSTER_HISTOGRAM)                     
            histogram(cluster.fig_25, cluster.no_of_particles_in_ith_cluster);
            set(cluster.fig_25,'xscale','log');
            set(cluster.fig_25,'yscale','log');       
            %set(cluster.fig_25,'xlim',[02 300]);   
            %set(cluster.fig_25,'ylim',[01 300]);
            
        end
        
        if(cluster.CLUSTER_PDF)
            if(length(cluster.no_of_particles_in_ith_cluster)>1)
                [cluster.pdf_cluster_mass{1}, cluster.pdf_cluster_mass{2}] = pdf(cluster.fig_26, cluster.no_of_particles_in_ith_cluster);
            else
                cluster.pdf_cluster_mass{1}=0; 
                cluster.pdf_cluster_mass{2}=0;
            end
            set(cluster.fig_26,'xscale','log');
            set(cluster.fig_26,'yscale','log'); 
            %cluster.avg_cluster_mass_M2byM1= M2byM1(cluster.pdf_cluster_mass{1},cluster.pdf_cluster_mass{2});
            cluster.avg_cluster_mass        = avgclustermass(cluster.no_of_particles_in_ith_cluster);
            cluster.avg_cluster_size        = cluster.avg_cluster_mass^(1/cluster.Df_fit);            
        end

        if(cluster.AVG_CLUSTER_TEMP)          
            p = plot(cluster.fig_33,time,mean(cluster.T_cls),'^b','linewidth',1);  
            Tg_haff    = data.T_HAFF_YSHIFT*(data.TAU_INV*time)^(data.HAFFS_EXPONENT); %+ 3.268*0.01* (TAU_INV*time)^(-11/6);
            gtemp1     = plot(cluster.fig_33,time,Tg_haff,'ok','markerfacecolor','k','markersize',5);
            leg        = legend(cluster.fig_33,[p,gtemp1],'aggregate temperature','Haff');
            set(leg,'interpreter','latex','box','off');             
        end      
        
        if(cluster.PLOT_CLUSTER_NUMBER_DENSITY)
            p = plot(cluster.fig_32,time,2*cluster.no_of_clusters/70^3,'sr','linewidth',1);
            
            n_simulation(iter) = 2*cluster.no_of_clusters/70^3;
            
            avg_no_of_clusters               = data.N_PARTICLE/cluster.avg_cluster_mass;

            p = plot(cluster.fig_32,time,avg_no_of_clusters/70^3,'^b','linewidth',1);
            
            n_simulation_mean_field(iter)    = avg_no_of_clusters/70^3;            
        end
        
        if(cluster.PLOT_CLUSTER_CHARGE_VAR)
        qq=chargeVsTime(cluster.fig_31,cluster.q_cls,time);        
        dqq_simulation(iter)= qq;
        end        
                       
        if(cluster.CLUSTER_RADIUS_OF_GYRATION)       
        plot(cluster.fig_35,time,mean(cluster.Rg),'sr','linewidth',1);
        y= 0.015*time^1.5;
        plot(cluster.fig_35,time,y,'+');
        end
        
        if(cluster.AVG_FRACTAL_DIM)           
            F = polyfit(log(cluster.no_of_particles_in_ith_cluster),log(cluster.Rg),1);
            slope     = F(1);
            intercept = F(2); 
            Df_fit    = 1/slope;
            plot(cluster.fig_27,time,Df_fit,'^b','linewidth',1);  
            %errorbar(fig_27,time,mean(Df),mean(Df)-min(Df),max(Df)-mean(Df),'^b','linewidth',1);               
            plot(cluster.fig_27,time,cluster.Df(cluster.cid_maxsize),'sr','linewidth',1); 
        end
                
        if(cluster.AVG_CLUSTER_SIZE)
            plot(cluster.fig_28,time,cluster.avg_cluster_size,'o'); 
            y= 0.015*time^1.5;
            plot(cluster.fig_28,time,y,'+');
            fprintf('avg. cluster size=%f\n',cluster.avg_cluster_size);
        end
        
        if(cluster.FRACTAL_DIMENSION_HISTOGRAM)            
            histogram(cluster.fig_30,cluster.Df)
        end

        
        if(cluster.CHARGE_PDF_ALL_PARTICLES)
            [cluster.pdf_charge_monomers{1}, cluster.pdf_charge_monomers{2}] = pdf(cluster.fig_39, data.charge);
             cluster.var_charge_monomers = var(data.charge);
        end
        
        if(cluster.CHARGE_PDF_NEWBORN_PARTICLES)
            if(length(cluster.no_of_particles_in_ith_cluster)>1)
                [cluster.pdf_charge_clusters{1}, cluster.pdf_charge_clusters{2}] = pdf(cluster.fig_36, cluster.q_cls);    
                 cluster.var_charge_clusters = var(cluster.q_cls);
            else
                cluster.pdf_charge_clusters{1}=0;
                cluster.pdf_charge_clusters{2}=0;    
                cluster.var_charge_clusters   =0;
            end
        end
               
        if(cluster.CHARGE_PDF_LARGEST_CLUSTER)
            if(length(cluster.no_of_particles_in_ith_cluster)>1)
            [cluster.pdf_charge_largest_cluster{1}, cluster.pdf_charge_largest_cluster{2}] ...
                = pdf(cluster.fig_38, cluster.clusters{8,cluster.cid_maxsize}(cluster.clusters{4,cluster.cid_maxsize}));
            else
                cluster.pdf_charge_largest_cluster{1}=0;
                cluster.pdf_charge_largest_cluster{2}=0;
            end
        end
               
        if(cluster.CLUSTER_K)
            cluster.K_clusters = 1.0*cluster.var_charge_clusters/cluster.T_cls/cluster.avg_cluster_size;
            plot(cluster.fig_34,time,cluster.K_clusters,'sr','linewidth',1);
        end
        
        if(cluster.CHARGE_ORDER_PARAMETER)
            q_Order = mean(abs(q_cls));
            plot(cluster.fig_cls_01,time,q_Order,'sr','linewidth',1);
        end
        
       
        
end