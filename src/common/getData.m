function [energy,density,charge,positionX,positionY,positionZ, velocityX,velocityY,velocityZ,forceX,forceY,forceZ] = ...
    getData(base_directory, timestring, FLOATTYPE,density_switch,charge_switch,force_switch,energy_switch)

    global CELL_LENGTH;
    global DIM3;
    global N_PARTICLE;

    if(energy_switch == 1)
        energy = getVector(base_directory, 'energy_t', timestring, FLOATTYPE);
    else
        energy = zeros(N_PARTICLE,1);
    end    
    
    if(density_switch == 1)
        density = getVector(base_directory, 'density_t', timestring, FLOATTYPE);
    else
        density = ones(N_PARTICLE,1);
    end
    
    if(charge_switch == 1)
        charge = getVector(base_directory, 'charge_t', timestring, FLOATTYPE);
    else
        charge = zeros(N_PARTICLE,1);
    end
    
    if(force_switch == 1)
        forceX = getVector(base_directory, 'forceX_t', timestring, FLOATTYPE);
        forceY = getVector(base_directory, 'forceY_t', timestring, FLOATTYPE);
        forceZ = getVector(base_directory, 'forceZ_t', timestring, FLOATTYPE);
    else
        forceX = zeros(N_PARTICLE,1);
        forceY = zeros(N_PARTICLE,1);
        forceZ = zeros(N_PARTICLE,1);
    end
    %----------------------------------------------------------------------
    positionX = getVector(base_directory, 'positionX_t', timestring, FLOATTYPE);
    positionX = CELL_LENGTH*positionX;
    positionY = getVector(base_directory, 'positionY_t', timestring, FLOATTYPE);
    positionY = CELL_LENGTH*positionY;
    if(DIM3==1)
        positionZ = getVector(base_directory, 'positionZ_t', timestring, FLOATTYPE);
        positionZ = CELL_LENGTH*positionZ;
    else
        positionZ = zeros(N_PARTICLE,1);
    end
    %----------------------------------------------------------------------
    velocityX = getVector(base_directory, 'velocityX_t', timestring, FLOATTYPE);
    velocityY = getVector(base_directory, 'velocityY_t', timestring, FLOATTYPE);
    if(DIM3==1)
        velocityZ = getVector(base_directory, 'velocityZ_t', timestring, FLOATTYPE);
    else
        velocityZ  = zeros(N_PARTICLE,1);
    end

    
end