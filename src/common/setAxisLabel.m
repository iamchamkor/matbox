function setAxisLabel(fig_handle, x_label, y_label, font_size, x_scale, y_scale)

set(get(fig_handle,'xlabel'),'string',x_label,'fontsize', font_size,...
    'FontWeight', 'normal', 'FontName', 'Times','Interpreter', 'Latex','rot',00);
set(get(fig_handle,'ylabel'),'string',y_label,'fontsize', font_size,...
    'FontWeight', 'normal', 'FontName', 'Times','Interpreter', 'Latex','rot',90);

set(fig_handle,'xscale',x_scale);
set(fig_handle,'yscale',y_scale); 

end
