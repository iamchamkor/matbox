function [] = parameter_space(obj)

close all;
set(0,'DefaultFigureVisible','on');
cd '/scratch.local/data/singh/dev/matbox/out/Nc_3';

phi_s=["0p002", "0p0036", "0p007", "0p013", "0p030", "0p076", "0p137",  "0p213"];
k_s  =["0p001", "0p0036", "0p01",  "0p023", "0p0625", "0p125", "0p4",   "1p0", "5p0"];

phi=[0.002 0.0036 0.007 0.013 0.030 0.076 0.137 0.213];
k  =[0.001 0.0036 0.01 0.023 0.0625 0.125 0.4 1.0 5.0];

[PHI,K]=meshgrid(phi,k);

[fig_1,fig_1_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','linlin',18);
[fig_2,fig_2_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','linlin',18);
% [fig_3,fig_3_gcf]  = createFig(1,         '',    '', 'holdon',   'boxon','linlin',18);
[fig_4,fig_4_gcf]  = createFig(1,         '$tv_\textrm{ref}/d$',    '$\langle D_f \rangle$', 'holdon',   'boxon','linlin',11);

li=0;
l1_handle={};
l1_name={};
Df_all = {};
t_all  = {};

markers = {'o','s','d','.','x','s','d','^','v','>','<','p','h'};
colors  = {[0.3010, 0.7450, 0.9330], [1, 0.2, 0.6], [0, 0, 1.0]}

i_start = 5;
i_end   = 5;

j_start = 1;
j_end   = 9;

for i=i_start:i_end
for j=j_start:j_end

        
        li=li+1;
        
        outdir = sprintf('phi_%s_K_%s',phi_s(i),k_s(j));    
        if exist(outdir,'dir')
            cd(outdir);
            pwd            
            
            load t;
            
            color_grad = 1 - (i-i_start)/(i_end-i_start);
            
            R = rand(1)
            G = rand(1)
            B = color_grad
            
            color = [R,G,B];
            

            load pdf_charge_clusters;
            load t;
            y=pdf_charge_clusters;


%             l1_handle{li} = plot(fig_1,t,y,'o','color',color,'markersize',8, 'linewidth', 1.5); 
%             l1_name{li}   = sprintf('$\\phi=%.3f$',phi(i));
%                         
% %             set(fig_1,'xlim',[10^-2 10^5]);
% %             set(fig_1,'ylim',[10^-6 10^1]);
%             set(fig_1,'xscale','log');
%             set(fig_1,'yscale','log');


            time_iter = 20;
            load t;
            t(time_iter)
            
            
            time=t(time_iter)
            y_gauss  = normpdf(y{time_iter,1});
            p1=plot(fig_1,y{time_iter,1},y_gauss,'-k','linewidth',2);
            p2=plot(fig_1,y{time_iter,1},y{time_iter,2},'.b','linewidth',1.0,'markersize',10);

            set(fig_1,'xlim',[-7 7]);
            set(fig_1,'ylim',[10^-5 10^0]);
            set(fig_1,'xscale','lin');
            set(fig_1,'yscale','log');

            cd('..');
        end
        
    end
end



time_iter = 20;
time=t(time_iter)
p4=plot(fig_1,y{time_iter,1},y{time_iter,2},'.c','linewidth',1.0,'markersize',10);

leg = legend(fig_1,[p2,p3,p4],'$t=5$','28','836');    
set(leg,'interpreter','latex','box','off','fontsize',14,'location','northwest');

set(fig_1,'xlim',[-8 8]);
set(fig_1,'ylim',[10^-5 10^1]);

xlabel(fig_1,'$\tilde{q}$');
ylabel(fig_1,'$f(\tilde{q})$');

txt=text(fig_1,1.5,0.8,'$Clusters$');
set(txt,'interpreter','latex','fontsize',15);

end