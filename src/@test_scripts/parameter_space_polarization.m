function [] = parameter_space_polarization(obj)

close all;
set(0,'DefaultFigureVisible','on');

cd '/scratch.local/data/singh/dev/matbox/out-polarization/poppe_exchange/Nc_2_frictinv';
%cd '/scratch.local/data/singh/dev/matbox/out-polarization';

pconst_s   =["0p0", "0p075"];
frictinv_s =["0p0", "0p05"];

pconst     =[0.0 0.075];
frictinv   =[0.0 0.05];

li=0;
l1_handle={};
l1_name={};


[fig_1,fig_1_gcf] = createFig(1, '$tv_\mathrm{ref}/d_0$', '$T$', 'holdon','boxon','loglog',18);
[fig_2,fig_2_gcf] = createFig(1, '$tv_\mathrm{ref}/d_0$', '$S(t)$', 'holdon','boxon','loglog',18);
[fig_3,fig_3_gcf] = createFig(1, '$tv_\mathrm{ref}/d_0$', '$\langle D_f\rangle$', 'holdon','boxon','loglin',18);
[fig_4,fig_4_gcf] = createFig(1, '$tv_\mathrm{ref}/d_0$', '$\langle \delta \mu^2\rangle$', 'holdon','boxon','linlin',18);

[fig_5,fig_5_gcf] = createFig(1, 'aggregate mass $m$',                 'mag. of dipole moment $\bf{\mu}$', 'holdon','boxon','linlin',18);
[fig_6,fig_6_gcf] = createFig(1, 'aggregate mass $m$',                 'mag. of dipole moment $\bf{\mu}$', 'holdon','boxon','linlin',18);
[fig_7,fig_7_gcf] = createFig(1, 'aggregate mass $m$',                 'mag. of dipole moment $\bf{\mu}$', 'holdon','boxon','linlin',18);

[fig_8,fig_8_gcf]   = createFig(1, 'aggregate mass $m$',               'aggregate radius of gyr. $R_{g}$', 'holdon','boxon','linlin',18);
[fig_9,fig_9_gcf]   = createFig(1, 'aggregate mass $m$',               'aggregate radius of gyr. $R_{g}$', 'holdon','boxon','linlin',18);
[fig_10,fig_10_gcf] = createFig(1, 'aggregate mass $m$',               'aggregate radius of gyr. $R_{g}$', 'holdon','boxon','linlin',18);

for i=2:length(pconst_s)
for j=1:length(frictinv_s)

        li=li+1;

        %outdir = sprintf('pconst_%s_frictinv_%s_contact_area_C_10_gamma_0p1',pconst_s(i),frictinv_s(j));
        outdir = sprintf('pconst_%s_frictinv_%s_blum_exchange',pconst_s(i),frictinv_s(j));    
        if exist(outdir,'dir')
            cd(outdir);
            pwd            
            
            load t;
            color = [rand(1), rand(1), rand(1)]
            
            %|T|-----------------------------------------------------------            
            load T_clusters;
            y = T_clusters;                        
            l1_handle{li} = plot(fig_1,t,y,'s','color',color,'markersize',6, 'linewidth', 1.2); 
            %l1_name{li}   = sprintf('$\\mathcal{A}=%.3f$',pconst(i));    
            l1_name{li}   = sprintf('$\\mathcal{S}=%.3f$',frictinv(j)); 
            set(fig_1,'xlim',[10^1 1.5*10^3]);
            set(fig_1,'ylim',[1.5*10^-6 5]);
            set(fig_1,'xscale','log');
            set(fig_1,'yscale','log');
            fig_1.XTick = [0.01 1 100 10000];
            fig_1.YTick = [1e-5 1e-4 1e-3 1e-2 1e-1 1e-0 1e1];
            
            %|S|-----------------------------------------------------------            
            load avg_cluster_size;
            y = avg_cluster_size;                        
            l2_handle{li} = plot(fig_2,t,y,'s','color',color,'markersize',6, 'linewidth', 1.2); 
            %l2_name{li}   = sprintf('$\\mathcal{A}=%.3f$',pconst(i));                        
            l2_name{li}   = sprintf('$\\mathcal{S}=%.3f$',frictinv(j));
            %set(fig_2,'xlim',[100 1400]);
            %set(fig_2,'ylim',[1.5 30]);
            set(fig_2,'xscale','lin');
            set(fig_2,'yscale','lin');
            %fig_2.XTick = [10 100 1000];
            %fig_2.YTick = [2e0 4e0 8e0 16e0 32e0];            
            
            %|Df|-----------------------------------------------------------            
            load Df_fit;
            y = Df_fit;                        
            l3_handle{li} = plot(fig_3,t,y,'d','color',color,'markersize',6, 'linewidth', 1.2); 
%             l3_name{li}   = sprintf('$\\mathcal{A}=%.3f$',pconst(i));    
            l3_name{li}   = sprintf('$\\mathcal{S}=%.3f$',frictinv(j));
            set(fig_3,'xlim',[0 1200]);
            set(fig_3,'ylim',[1   3]);
            set(fig_3,'xscale','lin');
            set(fig_3,'yscale','lin');
            fig_3.XTick = [0 200 400 600 800 1000 1200];
            fig_3.YTick = [0.5 1 1.5 2 2.5 3];          

            %|var_mom|-----------------------------------------------------------            
            load var_mom_cls;
            y = var_mom_cls;                        
            l4_handle{li} = plot(fig_4,t,y,'d','color',color,'markersize',6, 'linewidth', 1.2); 
%             l4_name{li}   = sprintf('$\\mathcal{A}=%.3f$',pconst(i)); 
            l4_name{li}   = sprintf('$\\mathcal{S}=%.3f$',frictinv(j));
%             set(fig_4,'xlim',[300 1200]);
%             set(fig_4,'ylim',[0 3]);
            set(fig_4,'xscale','lin');
            set(fig_4,'yscale','lin');
            %fig_4.XTick = [0 200 400 600 800 1000 1200];
            %fig_4.YTick = [0.5 1 1.5 2 2.5 3];      
            
            
            %|Rg vs mom|-----------------------------------------------------------            
            load mom_cls_abs;
            load m;                       
            tn = 8;
            t(tn)
            l5_handle{li} = plot(fig_5,m{tn},mom_cls_abs{tn},'o','color',color,'markersize',4, 'linewidth', 0.8); 
%             l5_name{li}   = sprintf('$\\mathcal{A}=%.3f$',pconst(i));
            l5_name{li}   = sprintf('$\\mathcal{S}=%.3f$',frictinv(j));
            l5_time       = sprintf('$t^*=%.1f$',t(tn));
            set(fig_5,'xlim',[1.0 1000]);
            set(fig_5,'ylim',[1e-6 1e-1]);
            set(fig_5,'xscale','log');
            set(fig_5,'yscale','log');
            %fig_4.XTick = [0 200 400 600 800 1000 1200];
            %fig_4.YTick = [0.5 1 1.5 2 2.5 3];                 
            
            tn = 20;
            t(tn)
            l6_handle{li} = plot(fig_6,m{tn},mom_cls_abs{tn},'o','color',color,'markersize',4, 'linewidth', 0.8); 
%             l6_name{li}   = sprintf('$\\mathcal{A}=%.3f$',pconst(i));  
            l6_name{li}   = sprintf('$\\mathcal{S}=%.3f$',frictinv(j));
            l6_time       = sprintf('$t^*=%.1f$',t(tn));
            set(fig_6,'xlim',[1.0 1000]);
            set(fig_6,'ylim',[1e-6 1e-1]);
            set(fig_6,'xscale','log');
            set(fig_6,'yscale','log');
            %fig_4.XTick = [0 200 400 600 800 1000 1200];
            %fig_4.YTick = [0.5 1 1.5 2 2.5 3];   
            
            tn = 29;
            t(tn)
            l7_handle{li} = plot(fig_7,m{tn},mom_cls_abs{tn},'o','color',color,'markersize',4, 'linewidth', 0.8); 
%             l7_name{li}   = sprintf('$\\mathcal{A}=%.3f$',pconst(i));   
            l7_name{li}   = sprintf('$\\mathcal{S}=%.3f$',frictinv(j));
            l7_time       = sprintf('$t^*=%.1f$',t(tn));
            set(fig_7,'xlim',[1.0 1000]);
            set(fig_7,'ylim',[1e-6 1e-1]);
            set(fig_7,'xscale','log');
            set(fig_7,'yscale','log');
            %fig_4.XTick = [0 200 400 600 800 1000 1200];
            %fig_4.YTick = [0.5 1 1.5 2 2.5 3];   
            
            
            %|m vs Rg|-----------------------------------------------------------        
            load Rg;
            load m;          
            tn = 8;
            t(tn)                           
            l8_handle{li} = plot(fig_8,m{tn},Rg{tn},'o','color',color,'markersize',4, 'linewidth', 0.8); 
%             l8_name{li}   = sprintf('$\\mathcal{A}=%.3f$',pconst(i));   
            l8_name{li}   = sprintf('$\\mathcal{S}=%.3f$',frictinv(j));
            l8_time       = sprintf('$t^*=%.1f$',t(tn));
            set(fig_8,'xlim',[1.0 1000]);
            set(fig_8,'ylim',[1e-1 1e1]);
            set(fig_8,'xscale','log');
            set(fig_8,'yscale','log');
            %fig_8.XTick = [0 200 400 600 800 1000 1200];
            %fig_8.YTick = [0.5 1 1.5 2 2.5 3];  
            
            tn = 20;
            t(tn)                           
            l9_handle{li} = plot(fig_9,m{tn},Rg{tn},'o','color',color,'markersize',4, 'linewidth', 0.8); 
%             l9_name{li}   = sprintf('$\\mathcal{A}=%.3f$',pconst(i));   
            l9_name{li}   = sprintf('$\\mathcal{S}=%.3f$',frictinv(j));
            l9_time       = sprintf('$t^*=%.1f$',t(tn));
            set(fig_9,'xlim',[1.0 1000]);
            set(fig_9,'ylim',[1e-1 1e1]);
            set(fig_9,'xscale','log');
            set(fig_9,'yscale','log');
            %fig_9.XTick = [0 200 400 600 800 1000 1200];
            %fig_9.YTick = [0.5 1 1.5 2 2.5 3];  
            
            tn = 29;
            t(tn)                           
            l10_handle{li} = plot(fig_10,m{tn},Rg{tn},'o','color',color,'markersize',4, 'linewidth', 0.8); 
%             l10_name{li}   = sprintf('$\\mathcal{A}=%.3f$',pconst(i));   
            l10_name{li}   = sprintf('$\\mathcal{S}=%.3f$',frictinv(j));
            l10_time       = sprintf('$t^*=%.1f$',t(tn));
            set(fig_10,'xlim',[1.0 1000]);
            set(fig_10,'ylim',[1e-1 1e1]);
            set(fig_10,'xscale','log');
            set(fig_10,'yscale','log');
            %fig_10.XTick = [0 200 400 600 800 1000 1200];
            %fig_10.YTick = [0.5 1 1.5 2 2.5 3];              
            
            %|end|---------------------------------------------------------
            cd('..');
        end
        
    end
end

 %|T|-----------------------------------------------------------
tt = 3:1:3000;
plot(fig_1,tt,0.017.*10^2.*tt.^(-5/3),'-k','linewidth',2);
txt=text(fig_1,7.6,0.00015,'$-5/3$');
set(txt,'interpreter','latex','fontsize',18);
leg = legend(fig_1,l1_name);    
set(leg,'interpreter','latex','box','off','fontsize',16,'location','northwest');

 %|S|-----------------------------------------------------------
leg = legend(fig_2,l2_name);    
set(leg,'interpreter','latex','box','off','fontsize',16,'location','northwest');
%tt = 1:1:1200;
%plot(fig_2,tt,exp(tt./100),'-k','linewidth',2);

 %|Df|-----------------------------------------------------------
leg = legend(fig_3,l3_name);    
set(leg,'interpreter','latex','box','off','fontsize',16,'location','south');

 %var_mom|-----------------------------------------------------------
leg = legend(fig_4,l4_name);    
set(leg,'interpreter','latex','box','off','fontsize',16,'location','south');


 %Rg vs mom|-----------------------------------------------------------
leg = legend(fig_5,l5_name);    
set(leg,'interpreter','latex','box','off','fontsize',16,'location','northeast');
txt=text(fig_5,90,0.004,l5_time);
set(txt,'interpreter','latex','fontsize',18);

leg = legend(fig_6,l6_name);    
set(leg,'interpreter','latex','box','off','fontsize',16,'location','northeast');
txt=text(fig_6,90,0.004,l6_time);
set(txt,'interpreter','latex','fontsize',18);

leg = legend(fig_7,l7_name);    
set(leg,'interpreter','latex','box','off','fontsize',16,'location','northeast');
txt=text(fig_7,90,0.004,l7_time);
set(txt,'interpreter','latex','fontsize',18);

 %m vs Rg|-----------------------------------------------------------
mm = 2:1:600;
plot(fig_8,mm,0.1.*mm.^(1),'-k','linewidth',2);
txt=text(fig_8,11,0.4,'$1$');
set(txt,'interpreter','latex','fontsize',18);
%
leg = legend(fig_8,l8_name);    
set(leg,'interpreter','latex','box','off','fontsize',16,'location','southeast');
txt=text(fig_8,3,0.11,l8_time);
set(txt,'interpreter','latex','fontsize',18);

mm = 2:1:600;
plot(fig_9,mm,0.1.*mm.^(1),'-k','linewidth',2);
txt=text(fig_9,11,0.4,'$1$');
set(txt,'interpreter','latex','fontsize',18);
%
leg = legend(fig_9,l9_name);    
set(leg,'interpreter','latex','box','off','fontsize',16,'location','southeast');
txt=text(fig_9,3,0.11,l9_time);
set(txt,'interpreter','latex','fontsize',18);

mm = 2:1:600;
plot(fig_10,mm,0.1.*mm.^(1),'-k','linewidth',2);
txt=text(fig_10,11,0.4,'$1$');
set(txt,'interpreter','latex','fontsize',18);
%
leg = legend(fig_10,l10_name);    
set(leg,'interpreter','latex','box','off','fontsize',16,'location','southeast');
txt=text(fig_10,3,0.11,l10_time);
set(txt,'interpreter','latex','fontsize',18);

end