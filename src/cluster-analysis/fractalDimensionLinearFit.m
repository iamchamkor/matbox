function [Df_fit,Rg_fit] = fractalDimensionLinearFit(no_of_particles_in_ith_cluster, Rg)
            
        F = polyfit(log(no_of_particles_in_ith_cluster),log(Rg),1);
        slope     = F(1);
        intercept = F(2);     
        
        Df_fit = 1/slope;
                
        Rg_fit = no_of_particles_in_ith_cluster.^(1/Df_fit).*exp(intercept);                        
            
end            