function [] = chargeFluctuationsOnSingleParticle(observables, data, fig_handle, time)
    
        N_PARTICLE  = data.N_PARTICLE;
        
        CELL_LENGTH = data.CELL_LENGTH;
        
        VIEW_ANGLE  = data.VIEW_ANGLE;
    
        q1 = data.charge(100);

        plot(fig_handle,time,q1,'.b');
    
end