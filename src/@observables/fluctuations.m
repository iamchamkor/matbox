function [w] = fluctuations(observables, data, fig_handle, time)

    N_PARTICLE = data.N_PARTICLE;    
    CELL_LENGTH= data.CELL_LENGTH;
    
    n_h = (N_PARTICLE/CELL_LENGTH^3);
    V_h = 0;
    T_h = 1;
    Q_h = 0;

if(fig_handle~=0)
    
        nboxes=10;
        
        np_box=zeros(nboxes+1, nboxes+1, nboxes+1);  
        
        momentumX_box=zeros(nboxes+1, nboxes+1, nboxes+1);
        momentumY_box=zeros(nboxes+1, nboxes+1, nboxes+1);
        momentumZ_box=zeros(nboxes+1, nboxes+1, nboxes+1);        
        
        fluctuating_velocityX = zeros(N_PARTICLE, 1);
        fluctuating_velocityY = zeros(N_PARTICLE, 1);
        fluctuating_velocityZ = zeros(N_PARTICLE, 1);
        
        if(data.charge_switch)
        fluctuating_charge      = zeros(N_PARTICLE, 1);        
        charge_box              = zeros(nboxes+1,nboxes+1,nboxes+1);
        charge_fluctuations_box = zeros(nboxes+1,nboxes+1,nboxes+1);
        end
        
        velocityX_fluctuations_box = zeros(nboxes+1,nboxes+1,nboxes+1);
        velocityY_fluctuations_box = zeros(nboxes+1,nboxes+1,nboxes+1);
        velocityZ_fluctuations_box = zeros(nboxes+1,nboxes+1,nboxes+1);
                        
        for(pid=1:N_PARTICLE)
            ibox=floor( (data.positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (data.positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (data.positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes)
                fprintf('problem in binning!')
            else
                np_box(ibox+1,jbox+1,kbox+1)        = np_box(ibox+1,jbox+1,kbox+1) + 1;
                momentumX_box(ibox+1,jbox+1,kbox+1) = momentumX_box(ibox+1,jbox+1,kbox+1) + 1*data.velocityX(pid);
                momentumY_box(ibox+1,jbox+1,kbox+1) = momentumY_box(ibox+1,jbox+1,kbox+1) + 1*data.velocityY(pid);
                momentumZ_box(ibox+1,jbox+1,kbox+1) = momentumZ_box(ibox+1,jbox+1,kbox+1) + 1*data.velocityZ(pid);
                if(data.charge_switch)
                charge_box(ibox+1,jbox+1,kbox+1)    = charge_box(ibox+1,jbox+1,kbox+1) + data.charge(pid);
                end
            end
        end
        
        vol_particle = (1/6)*pi*1^3;        
        
        vol_box      = (CELL_LENGTH/nboxes)^3;

        com_numberDensity_box = np_box./vol_box; %n
        com_velocityX_box     = momentumX_box./np_box; %Vx
        com_velocityY_box     = momentumY_box./np_box; %Vy
        com_velocityZ_box     = momentumZ_box./np_box; %Vz 
        if(data.charge_switch)
        com_charge_box        = charge_box./np_box;               
        end
        
        com_velocityX_box(isnan(com_velocityX_box))=0;
        com_velocityY_box(isnan(com_velocityY_box))=0;
        com_velocityZ_box(isnan(com_velocityZ_box))=0;
        if(data.charge_switch)
        com_charge_box(isnan(com_charge_box))      =0;
        end
        
        for(pid=1:N_PARTICLE)
            ibox=floor( (data.positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (data.positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (data.positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes)
                fprintf('problem in binning!')
            else
                fluctuating_velocityX(pid) = data.velocityX(pid) - com_velocityX_box(ibox+1,jbox+1,kbox+1);
                fluctuating_velocityY(pid) = data.velocityY(pid) - com_velocityY_box(ibox+1,jbox+1,kbox+1);
                fluctuating_velocityZ(pid) = data.velocityZ(pid) - com_velocityZ_box(ibox+1,jbox+1,kbox+1);                     

                if(data.charge_switch) 
                fluctuating_charge(pid)    = data.charge(pid)    - com_charge_box(ibox+1,jbox+1,kbox+1);
                end
                
                velocityX_fluctuations_box(ibox+1,jbox+1,kbox+1) = velocityX_fluctuations_box(ibox+1,jbox+1,kbox+1) + ( data.velocityX(pid)    - com_velocityX_box(ibox+1,jbox+1,kbox+1) )^2;
                velocityY_fluctuations_box(ibox+1,jbox+1,kbox+1) = velocityY_fluctuations_box(ibox+1,jbox+1,kbox+1) + ( data.velocityY(pid)    - com_velocityY_box(ibox+1,jbox+1,kbox+1) )^2;
                velocityZ_fluctuations_box(ibox+1,jbox+1,kbox+1) = velocityZ_fluctuations_box(ibox+1,jbox+1,kbox+1) + ( data.velocityZ(pid)    - com_velocityZ_box(ibox+1,jbox+1,kbox+1) )^2;                                
                if(data.charge_switch)     
                charge_fluctuations_box(ibox+1,jbox+1,kbox+1) = charge_fluctuations_box(ibox+1,jbox+1,kbox+1) + ( data.charge(pid)    - com_charge_box(ibox+1,jbox+1,kbox+1) )^2;
                end
                
            end
        end                             
        
        if(data.charge_switch)
        %charge-fluctuations
        dq2_particle = sum(fluctuating_charge.^2);
        dq2_particle = (dq2_particle)/N_PARTICLE;    
        q_T_particle = sqrt(dq2_particle);
        
        dq2_box  = sum(sum(sum((charge_fluctuations_box))));
        dq2_box  = (dq2_box)/nboxes^3;  
        q_T_box  = sqrt(dq2_box);
        end

        %velocity-fluctuations
        dv2_particle = sum(fluctuating_velocityX.^2 + fluctuating_velocityY.^2 + fluctuating_velocityZ.^2);
        dv2_particle = (1/3)*(dv2_particle)/N_PARTICLE;
        v_T          = sqrt(3.0*dv2_particle/1.0);
                    
        %macroscopic perturbations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %scaled number density perturbation
        dn2_box  = (com_numberDensity_box - n_h).^2;
        dn2_box  = sum(sum(sum((dn2_box))));
        dn2_box  = (dn2_box)/nboxes^3;
        
        dn  = sqrt(dn2_box);
        rho = dn/n_h; 

        %scaled temperature perturbation
        T_box    = (1/3)*(velocityX_fluctuations_box + velocityY_fluctuations_box + velocityZ_fluctuations_box);        
        T_avg    = sum(sum(sum(T_box)))/nboxes^3;       
        
        dT2_box  = (T_box - v_T^2).^2;
        dT2_box  = sum(sum(sum((dT2_box))));
        dT2_box  = (dT2_box)/nboxes^3;
        
        dT       = sqrt(dT2_box);
        theta    = dT/v_T^2;  
        
        %scaled velocity perturbation (or Mach number?)
        V2_box  = (com_velocityX_box.^2 + com_velocityY_box.^2 + com_velocityZ_box.^2);                               
        V2_box  = sum(sum(sum(V2_box)));
        V2_box  = (V2_box)/nboxes^3;
        w       = sqrt(V2_box)/v_T;              
        
        if(data.charge_switch)
        %scaled charge perturbation 
        Q2_box  = (com_charge_box.^2);
        Q2_box  = sum(sum(sum(Q2_box)));
        Q2_box  = (Q2_box)/nboxes^3;
        Q_tilde = sqrt(Q2_box)/q_T_particle;
        end
        
      
        %end macroscopic perturbations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    if(fig_handle~=0)
        
%         figure(fig_handle_gcf);
% 
%         sp_1        = subplot(2,2,1);
%         if(data.charge_switch)
%         gtemp1      = plot(sp_1,time,rho,'sr');
%         else
%         gtemp1      = plot(sp_1,time,rho,'sg');
%         end
%         setAxisLabel(sp_1,'time $t v_\textrm{ref}/d$', '$\rho=\frac{\langle(n-n_h)^2\rangle^{1/2}}{n_h}$', 16, 'lin', 'log');
%         hold(sp_1,'on');
%         
%         sp_2        = subplot(2,2,3);
%         if(data.charge_switch)
%         gtemp1      = plot(sp_2,time,theta,'sr');   
%         else
%         gtemp1      = plot(sp_2,time,theta,'sg'); 
%         end
%         setAxisLabel(sp_2,'time $t v_\textrm{ref}/d$', '$\theta=\frac{\langle(T-v_T^2)^2\rangle^{1/2}}{v_T^2}$', 16, 'lin', 'log');
%         hold(sp_2,'on');
% 
%         sp_3        = subplot(2,2,2);
%         if(data.charge_switch)
%         gtemp1      = plot(sp_3,time,w,'sr');  
%         else
%         gtemp1      = plot(sp_3,time,w,'sg'); 
%         end
%         setAxisLabel(sp_3,'time $t v_\textrm{ref}/d$', '$|w|=\frac{\langle{\bf V}^2\rangle^{1/2}}{v_T}$', 16, 'lin', 'log');
%         hold(sp_3,'on'); 
%         
%         sp_4        = subplot(2,2,4);
%         if(data.charge_switch)
%         gtemp1      = plot(sp_4,time,Q_tilde,'sr');  
%         else
%         gtemp1      = plot(sp_4,time,Q_tilde,'sg'); 
%         end
%         setAxisLabel(sp_4,'time $t v_\textrm{ref}/d$', '$\tilde{Q}=\frac{\langle Q^2\rangle^{1/2}}{q_T}$', 16, 'lin', 'log');
%         hold(sp_4,'on');  
        
        if(data.charge_switch)
        gtemp1      = plot(fig_handle,time,w,'sr','markersize',15);  
        else
        gtemp1      = plot(fig_handle,time,w,'sg'); 
        end
        setAxisLabel(fig_handle,'', '${Ma}$', 20, 'log', 'log'); %time $t v_\textrm{ref}/d$ %Ma=\frac{\langle{\bf V}^2\rangle^{1/2}}{v_T}
        hold(fig_handle,'on'); 


    end
        
end

end