classdef observables < handle
    
    properties
        
        OBSERVABLES;
        
        T_monomers=zeros();
        Ma=zeros();
        var_mom_mon;
        
        ke_sph=zeros();
        t_sph =zeros();
        
        pdf_charge_monomers={};
        var_charge_monomers=zeros();
        
        %|cluster-switches|also indicate exisisting functionalities|%%%%%%%%%%%
            VELOCITY_VECTORS;        fig_0;  fig_0_gcf;              
            PARTICLES_IN_BOX;        fig_1;  fig_1_gcf;
            PLOT_FORCES;             fig_2;  fig_2_gcf;
            PLOT_TEMPERATURE;        fig_3;  fig_3_gcf;
            PLOT_FLUCTUATIONS;       fig_4;  fig_4_gcf;
            PLOT_KE;                 fig_5;  fig_5_gcf;
            PLOT_DISSIPATION_RATE;   fig_7;  fig_7_gcf;
            PLOT_CHARGE_VS_TIME;     fig_6;  fig_6_gcf;
            PLOT_F_DIST;             fig_8;  fig_8_gcf;
            PLOT_V_DIST;             fig_9;  fig_9_gcf;
            PLOT_CHARGE_DIST;        fig_10; fig_10_gcf;
            PLOT_VABS_DIST;          fig_11; fig_11_gcf;
            PLOT_COLLISIONS;         fig_12; fig_12_gcf;
            SINGLE_PARTICLE_IN_BOX;  fig_13; fig_13_gcf;
            PLOT_MSD;                fig_16; fig_16_gcf;
            MIDPLANE_DENSITY_SPH;    fig_36; fig_36_gcf;
            MIDPLANE_VELOCITY_SPH;   fig_37; fig_37_gcf;
            CHARGE_SEP_ORDER_PARA;   fig_38; fig_38_gcf;
            Q_FLUC_ONE_PARTICLE;     fig_39; fig_39_gcf;
            PLOT_KE_SPH;             fig_40; fig_40_gcf;
            PLOT_DISSIPATION;        fig_41; fig_41_gcf;
            PLOT_VAR_DIPOLE_MOMENT;  fig_42; fig_42_gcf;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
    end
    
    methods

    function [obj] = observables %class-constructor

        obj.OBSERVABLES=1;
        
        if(obj.OBSERVABLES)
        obj.VELOCITY_VECTORS      =0;[obj.fig_0,obj.fig_0_gcf]  = createFig(obj.VELOCITY_VECTORS,      'empty',                     'empty',                             'holdoff',  'boxon','linlin',14);     
        obj.PARTICLES_IN_BOX      =0;[obj.fig_1,obj.fig_1_gcf]  = createFig(obj.PARTICLES_IN_BOX,      'empty',                     'empty',                             'holdoff',  'boxon','linlin',14);
        obj.PLOT_FORCES           =0;[obj.fig_2,obj.fig_2_gcf]  = createFig(obj.PLOT_FORCES,           'Particle index',            '$F_z$',                             'holdon',   'boxon','linlin',14);
        obj.PLOT_TEMPERATURE      =0;[obj.fig_3,obj.fig_3_gcf]  = createFig(obj.PLOT_TEMPERATURE,      'time $t v_\textrm{ref}/d_0$', 'granular temperature $T$',          'holdon',   'boxon','loglog',14);
        obj.PLOT_FLUCTUATIONS     =0;[obj.fig_4,obj.fig_4_gcf]  = createFig(obj.PLOT_FLUCTUATIONS,     'time $t v_\textrm{ref}/d_0$', '$\langle\delta O^2\rangle$',        'holdon',   'boxon','loglog',18);
        obj.PLOT_KE               =0;[obj.fig_5,obj.fig_5_gcf]  = createFig(obj.PLOT_KE,               '$t/\tau$',                  '$E$',                               'holdon',   'boxon','linlin',14);
        obj.PLOT_DISSIPATION_RATE =0;[obj.fig_7,obj.fig_7_gcf]  = createFig(obj.PLOT_DISSIPATION_RATE, '$t/\tau$',                  '$-\frac{dE}{dt}$',                  'holdon',   'boxon','linlin',14);
        obj.PLOT_CHARGE_VS_TIME   =0;[obj.fig_6,obj.fig_6_gcf]  = createFig(obj.PLOT_CHARGE_VS_TIME,   'time $t v_\textrm{ref}/d_0$', '$\langle\delta q^2\rangle$',        'holdon',   'boxon','loglog',14);
        obj.PLOT_F_DIST           =0;[obj.fig_8,obj.fig_8_gcf]  = createFig(obj.PLOT_F_DIST,           '$F_x/<F_x^2>^{1/2}$',       '$P(F_x)$',                          'holdon',   'boxon','linlog',14);
        obj.PLOT_V_DIST           =0;[obj.fig_9,obj.fig_9_gcf]  = createFig(obj.PLOT_V_DIST,           '$v_x/<v_x^2>^{1/2}$',       '$P(v_x)$',                          'holdon',   'boxon','linlog',14);
        obj.PLOT_CHARGE_DIST      =0;[obj.fig_10,obj.fig_10_gcf]= createFig(obj.PLOT_CHARGE_DIST,      '$q/\langle\delta q^2\rangle^{1/2}$',           '$f(q)$',         'holdon',   'boxon','linlog',14);
        obj.PLOT_VABS_DIST        =0;[obj.fig_11,obj.fig_11_gcf]= createFig(obj.PLOT_VABS_DIST,        '$v/<v^2>^{1/2}$',           '$f(v)$',                            'holdon',   'boxon','linlog',14);
        obj.PLOT_COLLISIONS       =0;[obj.fig_12,obj.fig_12_gcf]= createFig(obj.PLOT_COLLISIONS,       '$t/\tau$',                  '$N_\mathrm{{stick}}$',              'holdon',   'boxon','linlin',14);
        obj.SINGLE_PARTICLE_IN_BOX=0;[obj.fig_13,obj.fig_13_gcf]= createFig(obj.SINGLE_PARTICLE_IN_BOX,'',                          '',                                  'holdon',   '',     'linlin',14);
        obj.PLOT_MSD              =0;[obj.fig_16,obj.fig_16_gcf]= createFig(obj.PLOT_MSD,              '$t/\tau$',                  '$\langle \Delta \bf{r}^2 \rangle$', 'holdon',   'boxon','loglog',14);
        obj.MIDPLANE_DENSITY_SPH  =0;[obj.fig_36,obj.fig_36_gcf]= createFig(obj.MIDPLANE_DENSITY_SPH,  'x',                         'y',                                 'holdoff',  'boxon','linlin',14);
        obj.MIDPLANE_VELOCITY_SPH =0;[obj.fig_37,obj.fig_37_gcf]= createFig(obj.MIDPLANE_VELOCITY_SPH, '$v_x$',                     '$z$',                               'holdon',   'boxon','linlin',14);
        obj.CHARGE_SEP_ORDER_PARA =0;[obj.fig_38,obj.fig_38_gcf]= createFig(obj.CHARGE_SEP_ORDER_PARA, '$t$',                       '$S$',                               'holdon',   'boxon','linlin',14);
        obj.Q_FLUC_ONE_PARTICLE   =0;[obj.fig_39,obj.fig_39_gcf]= createFig(obj.Q_FLUC_ONE_PARTICLE,   '$t$',                       '$q_i$',                             'holdon',   'boxon','linlin',14);
        obj.PLOT_KE_SPH           =0;[obj.fig_40,obj.fig_40_gcf]= createFig(obj.PLOT_KE_SPH,           'time $t v_\textrm{ref}/d_0$', '$E$',                               'holdon',   'boxon','linlin',14);
        obj.PLOT_DISSIPATION      =0;[obj.fig_41,obj.fig_41_gcf]= createFig(obj.PLOT_DISSIPATION,      'time $t v_\textrm{ref}/d_0$', '$E$',                               'holdon',   'boxon','linlin',14);
        obj.PLOT_VAR_DIPOLE_MOMENT=0;[obj.fig_42,obj.fig_42_gcf]= createFig(obj.PLOT_VAR_DIPOLE_MOMENT,     'time $t v_\textrm{ref}/d_0$', '$\langle\delta \mu^2\rangle$',      'holdon',   'boxon','loglog',14);
        end
        
    end 
    
    end
    
end