function [ke] = kineticEnergy(observables, data, fig_handle, time)
    
%     global N_PARTICLE;
%     global T_HAFF_YSHIFT;
%     global TAU_INV;
%     global HAFFS_EXPONENT;

        ke         = sum(0.5.*1.0.*(data.velocityX.^2 + data.velocityY.^2 + data.velocityZ.^2));
        ke         = sum(ke)/data.N_PARTICLE;
        
        %Tg_haff    = T_HAFF_YSHIFT*(TAU_INV*time)^(HAFFS_EXPONENT); %+ 3.268*0.01* (TAU_INV*time)^(-11/6);
        %kepp1 = plot(fig_handle,time,Tg_haff,'ok','markerfacecolor','k','markersize',2);
        
        plot(fig_handle,time,ke,'or');
        
        %leg   = legend(fig_handle,[kepp2],'Simulation');
    
end