function [] = midPlaneDensity(observables, data, fig_handle, time)

        N_PARTICLE  = data.N_PARTICLE;
        CELL_LENGTH = data.CELL_LENGTH;
        VIEW_ANGLE  = data.VIEW_ANGLE;
    
        nboxes=32;      
        
        np_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        density_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumX_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumY_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        momentumZ_box=zeros(nboxes+1,nboxes+1,nboxes+1);
        
        for(pid=1:N_PARTICLE)
            ibox=floor( (data.positionX(pid)/CELL_LENGTH + 0.5)*nboxes);
            jbox=floor( (data.positionY(pid)/CELL_LENGTH + 0.5)*nboxes);
            kbox=floor( (data.positionZ(pid)/CELL_LENGTH + 0.5)*nboxes);
            boxindex=ibox+jbox*nboxes+kbox*nboxes*nboxes;
            if(boxindex>nboxes*nboxes*nboxes)
                fprintf('problem in binning!')
            else
                np_box(ibox+1,jbox+1,kbox+1)        = np_box(ibox+1,jbox+1,kbox+1)        + 1;
                density_box(ibox+1,jbox+1,kbox+1)   = density_box(ibox+1,jbox+1,kbox+1)   + data.density(pid);
                momentumX_box(ibox+1,jbox+1,kbox+1) = momentumX_box(ibox+1,jbox+1,kbox+1) + data.velocityX(pid);
                momentumY_box(ibox+1,jbox+1,kbox+1) = momentumY_box(ibox+1,jbox+1,kbox+1) + data.velocityY(pid);
                momentumZ_box(ibox+1,jbox+1,kbox+1) = momentumZ_box(ibox+1,jbox+1,kbox+1) + data.velocityZ(pid);
            end
        end
        
        com_density_box   = density_box  ./np_box;
        com_momentumX_box = momentumX_box./np_box;
        com_momentumY_box = momentumY_box./np_box;
        com_momentumZ_box = momentumZ_box./np_box;      
        
        com_density_box(isnan(com_density_box))    =0;
        com_momentumX_box(isnan(com_momentumX_box))=0;
        com_momentumY_box(isnan(com_momentumY_box))=0;
        com_momentumZ_box(isnan(com_momentumZ_box))=0;        
       
    if(fig_handle~=0)
        %plot(fig_handle,time, sum(sum(sum( com_momentumX_box + com_momentumY_box + com_momentumZ_box ))) /(nboxes^3),'^k' );       

        
        [X,Y,Z] = meshgrid(-CELL_LENGTH/2:CELL_LENGTH/nboxes:CELL_LENGTH/2,...
                            -CELL_LENGTH/2:CELL_LENGTH/nboxes:CELL_LENGTH/2,...
                             -CELL_LENGTH/2:CELL_LENGTH/nboxes:CELL_LENGTH/2);
        
%         [X,Y,Z] = meshgrid(-pi:2*pi/nboxes:+pi,-pi:2*pi/nboxes:+pi,-pi:2*pi/nboxes:+pi);
                
%         [divV] = divergence(X,Y,Z,com_momentumX_box,com_momentumY_box,com_momentumZ_box);
%  
%         max(max(max(divV)))

%vabs = sqrt(com_momentumX_box.^2 + com_momentumY_box.^2 + com_momentumZ_box.^2);

 [curlx,curly,curlz,cav] = curl(X,Y,Z,com_momentumX_box,com_momentumY_box,com_momentumZ_box);        
%     
% cav = sum(sum(sum(0.5.*com_density_box(1:end-1,1:end-1,1:end-1).*cav(1:end-1,1:end-1,1:end-1).^2)))/(nboxes)^3;
% plot(time,cav,'ob');hold on;
% set(gca,'ylim',[0 0.016]);

% whos cav

variable = curlz;

p = patch( isosurface(X,Y,Z,variable,0.025));
isonormals(X,Y,Z,variable,p)
p.FaceColor = 'red';
p.EdgeColor = 'none';
daspect([1 1 1])
view(3); 
axis tight
camlight 
lighting gouraud    
hold('off');
        
set(fig_handle,'View',[-VIEW_ANGLE,VIEW_ANGLE]);
axis(fig_handle,[-CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2 -CELL_LENGTH/2 CELL_LENGTH/2]);

end

end