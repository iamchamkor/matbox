function [dr_msd,t_msd] = msd(fig_handle,positionX,positionY,positionZ,...
                                X_0,Y_0,Z_0,step,time)

    global N_PARTICLE;

    if(fig_handle~=0)
   
        dx = (positionX - X_0);
        dy = (positionY - Y_0);
        dz = (positionZ - Z_0);
        
        ds2 = (dx.^2 + dy.^2 + dz.^2);
        
        MSD = sum(ds2)/N_PARTICLE;
        
        plot(fig_handle,time,MSD,'.r','markersize',10);
        
        dr_msd(step) = MSD;
        t_msd(step) = time;
        
    else
    
        dr_msd=0;
        t_msd=0;
        
    end
    
end