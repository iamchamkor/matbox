classdef store < handle
    
    properties
        
        STORE;
        
        out_directory;
        
        %|properties/observables/variables to save|------------------------         

        t;

        no_of_clusters;
        m;
        Rg;                 
        Df_fit;                 % from fitting m vs Rg

        v_cls_abs;
        Alpha_fit;
        
        q_cls_abs;
        Beta_fit;
        
        mom_cls_abs;
        var_mom_cls;
        
        avg_cluster_mass;       % M2/M1 of pdf_cluster_mass
        avg_cluster_size;       % S ~ avg_cluster_mass_M2byM1^(1/Df_fit)

        pdf_charge_monomers;
        pdf_charge_clusters;
        pdf_charge_largest_cluster;
        %pdf_charge_individual_clusters_averaged;

        var_charge_monomers;
        var_charge_clusters;
        
        K_clusters;
                
        T_monomers;
        T_clusters;
        
        Ma; 
        var_mom_mon;
         
    end
    
    methods
        
            function [obj] = store(obj_data) %|class-constructor
                
            obj.STORE=0;
            
               %|store directory|------------------------------------------
               obj.out_directory = obj_data.out_directory;               
               if ~exist( obj.out_directory, 'dir')
                    mkdir(obj.out_directory);           
               else                    
                    delete(sprintf('%s/*.*',obj.out_directory));      
               end                                                                                                                          
            end
        
            function [istore] = store2memory(obj_store, obj_cluster, obj_observables, time, istore)                                
                obj_store.t(istore)                         = time;  

                
                if(obj_store.STORE)
                obj_store.no_of_clusters(istore)            = obj_cluster.no_of_clusters;                
                obj_store.m{istore}                         = obj_cluster.no_of_particles_in_ith_cluster;
                obj_store.Rg{istore}                        = obj_cluster.Rg;                
                obj_store.Df_fit(istore)                    = obj_cluster.Df_fit;

                obj_store.v_cls_abs{istore}                 = obj_cluster.v_cls_abs;                
                obj_store.Alpha_fit(istore)                 = obj_cluster.Alpha_fit;                
                
                obj_store.q_cls_abs{istore}                 = obj_cluster.q_cls_abs; 
                obj_store.Beta_fit(istore)                  = obj_cluster.Beta_fit;
                
                obj_store.mom_cls_abs{istore}               = obj_cluster.mom_cls_abs; 
                obj_store.var_mom_cls(istore)               = obj_cluster.var_mom_cls;
                
                obj_store.avg_cluster_mass(istore)          = obj_cluster.avg_cluster_mass;
                obj_store.avg_cluster_size(istore)          = obj_cluster.avg_cluster_size;
                
                obj_store.pdf_charge_monomers{istore,1}     = obj_cluster.pdf_charge_monomers{1};
                obj_store.pdf_charge_monomers{istore,2}     = obj_cluster.pdf_charge_monomers{2};
                
                obj_store.pdf_charge_clusters{istore,1}     = obj_cluster.pdf_charge_clusters{1};
                obj_store.pdf_charge_clusters{istore,2}     = obj_cluster.pdf_charge_clusters{2};
                
                obj_store.pdf_charge_largest_cluster{istore,1} = obj_cluster.pdf_charge_largest_cluster{1};
                obj_store.pdf_charge_largest_cluster{istore,2} = obj_cluster.pdf_charge_largest_cluster{2};
                
                obj_store.var_charge_monomers(istore)       = obj_cluster.var_charge_monomers;
                obj_store.var_charge_clusters(istore)       = obj_cluster.var_charge_clusters;
                
                obj_store.K_clusters(istore)                = obj_cluster.K_clusters;                
                obj_store.T_clusters(istore)                = obj_cluster.T_cls;
                
                
                
                end
                
                
                if(obj_store.STORE)
                obj_store.T_monomers(istore)                = obj_observables.T_monomers;                
                obj_store.Ma(istore)                        = obj_observables.Ma; 
                obj_store.var_mom_mon(istore)         = obj_observables.var_mom_mon;
                
%                 obj_store.pdf_charge_monomers{istore,1}     = obj_observables.pdf_charge_monomers{1};
%                 obj_store.pdf_charge_monomers{istore,2}     = obj_observables.pdf_charge_monomers{2};
%                 obj_store.var_charge_monomers(istore)       = obj_observables.var_charge_monomers;                
                end
                
                istore = istore+1;                
            end
            
            function [] = store2files(obj_store)                           
                                
                pause(1e-1);
                
                t                           = obj_store.t;                          save(sprintf('%s/t.mat'                             ,obj_store.out_directory),'t');                             clear t;
                no_of_clusters              = obj_store.no_of_clusters;             save(sprintf('%s/no_of_clusters.mat'                ,obj_store.out_directory),'no_of_clusters');                clear no_of_clusters;   
                m                           = obj_store.m;                          save(sprintf('%s/m.mat'                             ,obj_store.out_directory),'m');                             clear m;            
                Rg                          = obj_store.Rg;                         save(sprintf('%s/Rg.mat'                            ,obj_store.out_directory),'Rg');                            clear Rg;
                Df_fit                      = obj_store.Df_fit;                     save(sprintf('%s/Df_fit.mat'                        ,obj_store.out_directory),'Df_fit');                        clear Df_fit;

                v_cls_abs                   = obj_store.v_cls_abs;                  save(sprintf('%s/v_cls_abs.mat'                     ,obj_store.out_directory),'v_cls_abs');                     clear v_cls_abs;
                Alpha_fit                   = obj_store.Alpha_fit;                  save(sprintf('%s/Alpha_fit.mat'                     ,obj_store.out_directory),'Alpha_fit');                     clear Alpha_fit;
            
                q_cls_abs                   = obj_store.q_cls_abs;                  save(sprintf('%s/q_cls_abs.mat'                     ,obj_store.out_directory),'q_cls_abs');                     clear q_cls_abs;
                Beta_fit                    = obj_store.Beta_fit;                   save(sprintf('%s/Beta_fit.mat'                      ,obj_store.out_directory),'Beta_fit');                      clear Beta_fit;
                
                avg_cluster_mass            = obj_store.avg_cluster_mass;           save(sprintf('%s/avg_cluster_mass.mat'              ,obj_store.out_directory),'avg_cluster_mass');              clear avg_cluster_mass;
                avg_cluster_size            = obj_store.avg_cluster_size;           save(sprintf('%s/avg_cluster_size.mat'              ,obj_store.out_directory),'avg_cluster_size');              clear avg_cluster_size;
                
                pdf_charge_monomers         = obj_store.pdf_charge_monomers;        save(sprintf('%s/pdf_charge_monomers.mat'           ,obj_store.out_directory),'pdf_charge_monomers');           clear pdf_charge_monomers;
                pdf_charge_clusters         = obj_store.pdf_charge_clusters;        save(sprintf('%s/pdf_charge_clusters.mat'           ,obj_store.out_directory),'pdf_charge_clusters');           clear pdf_charge_clusters;
                pdf_charge_largest_cluster  = obj_store.pdf_charge_largest_cluster; save(sprintf('%s/pdf_charge_largest_cluster.mat'    ,obj_store.out_directory),'pdf_charge_largest_cluster');    clear pdf_charge_largest_cluster;
               
                var_charge_monomers         = obj_store.var_charge_monomers;        save(sprintf('%s/var_charge_monomers.mat'           ,obj_store.out_directory),'var_charge_monomers');           clear var_charge_monomers;
                var_charge_clusters         = obj_store.var_charge_clusters;        save(sprintf('%s/var_charge_clusters.mat'           ,obj_store.out_directory),'var_charge_clusters');           clear var_charge_clusters; 
                
                K_clusters                  = obj_store.K_clusters;                 save(sprintf('%s/K_clusters.mat'                    ,obj_store.out_directory),'K_clusters');                    clear K_clusters;                
                
                T_clusters                  = obj_store.T_clusters;                 save(sprintf('%s/T_clusters.mat'                    ,obj_store.out_directory),'T_clusters');                    clear T_clusters;
                T_monomers                  = obj_store.T_monomers;                 save(sprintf('%s/T_monomers.mat'                    ,obj_store.out_directory),'T_monomers');                    clear T_monomers;
                
                Ma                          = obj_store.Ma;                         save(sprintf('%s/Ma.mat'                            ,obj_store.out_directory),'Ma');                            clear Ma;
                
                var_mom_mon                 = obj_store.var_mom_mon;                save(sprintf('%s/var_mom_mon.mat'                   ,obj_store.out_directory),'var_mom_mon');                   clear var_mom_mon;
                mom_cls_abs                 = obj_store.mom_cls_abs;                save(sprintf('%s/mom_cls_abs.mat'                   ,obj_store.out_directory),'mom_cls_abs');                   clear mom_cls_abs;
                var_mom_cls                 = obj_store.var_mom_cls;                save(sprintf('%s/var_mom_cls.mat'                   ,obj_store.out_directory),'var_mom_cls');                   clear var_mom_cls;
            end
            
    end
    
end